package com.sand.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.sand.example.pages.LandingPage;
import com.sand.example.pages.QuestionsPage;
import com.sand.test.base.BaseTestNGTest;

public class ExampleTest extends BaseTestNGTest {

    public ExampleTest() {
        super();
    }

    @Test(groups = {"group1"})
    public void clickQuestionsTest() {
        LandingPage landingPage = new LandingPage(driver);
        QuestionsPage questionsPage = landingPage.clickQuestionsTab();
        Assert.assertTrue(questionsPage.isUsersTabDisplayed());
    }

    @Test(groups = {"group2"})
    public void isLogoDisplayedTest() {
        LandingPage landingPage = new LandingPage(driver);
        Assert.assertTrue(landingPage.isQuestionsTabDisplayed());
    }

}