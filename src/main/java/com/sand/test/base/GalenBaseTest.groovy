package com.sand.test.base

import static java.util.Arrays.asList

import java.lang.reflect.Method

import net.mindengine.galen.api.Galen
import net.mindengine.galen.reports.TestReport
import net.mindengine.galen.reports.model.LayoutReport

import org.openqa.selenium.Dimension
import org.openqa.selenium.WebDriver
import org.testng.annotations.BeforeMethod
import org.testng.annotations.DataProvider

import com.sand.base.galen.GalenReportsContainer;

public abstract class GalenBaseTest extends BaseTestNGTest {

	public void checkLayout(WebDriver driver, String specPath, List<String> includedTags) throws IOException {
		String title = "Check layout " + specPath
		LayoutReport layoutReport = Galen.checkLayout(driver, specPath, includedTags, null, new Properties(), null)
		report.get().layout(layoutReport, title)

		if (layoutReport.errors() > 0) {
			throw new RuntimeException("Incorrect layout: " + title)
		}
	}

	ThreadLocal<TestReport> report = new ThreadLocal<TestReport>()

	@BeforeMethod
	public void setReport(Method method) {
		//beforeMethod()
		report.set(GalenReportsContainer.get().registerTest(method))
	}

	@BeforeMethod
	public void setDeviceSize(Object[] args) {
		if (args.length > 0) {
			if (args[0] != null && args[0] instanceof TestDevice) {
				TestDevice device = (TestDevice)args[0]
				if (device.getScreenSize() != null) {
					driver.manage().window().setSize(device.getScreenSize())
				}
			}
		}
	}

	@DataProvider(name = "devices")
	public Object [][] devices () {
		return ([
			[new TestDevice("mobile", new Dimension(450, 800), asList("mobile"))],
			[new TestDevice("tablet", new Dimension(750, 800), asList("tablet"))],
			[new TestDevice("desktop", new Dimension(1024, 800), asList("desktop"))]
		] as Object[][])
	}


	public static class TestDevice {
		String name
		Dimension screenSize
		List<String> tags


		public String getName() {
			return name
		}

		public Dimension getScreenSize() {
			return screenSize
		}

		public List<String> getTags() {
			return tags
		}
	}

}