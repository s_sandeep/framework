package com.sand.base.driver
/**
 * The base class for creating a WebDriver instance based on whether the
 * requested driver is local, remote or mobile
 */

/**
 * @author SANDEEP
 *
 */
 
import groovy.util.ConfigObject

import com.sand.base.FrameworkConfig;

import java.nio.file.Path

import org.openqa.selenium.WebDriver


abstract class Driver {
	
	protected WebDriver driver
	protected ConfigObject config
	
	public Driver() {
		config = FrameworkConfig.getInstance().config
	}
	
	protected abstract WebDriver createDriver();
}
