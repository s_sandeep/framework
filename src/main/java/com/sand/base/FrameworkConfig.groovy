package com.sand.base;

import groovy.transform.Synchronized;
import groovy.util.ConfigObject;
import groovy.util.ConfigSlurper;

import java.io.File;
import java.net.MalformedURLException;

public class FrameworkConfig {
	
	private static FrameworkConfig instance
	public static ConfigObject config
		
	private FrameworkConfig() {
		try {
			config = new ConfigSlurper().parse(new File("src/resources/Config.groovy").toURI().toURL());
		} catch (MalformedURLException e) {
			System.out.println("unable to locate Config file " + instance.toString());
			e.printStackTrace();
		}
	}
	
	// creating a thread safe getInstance method using groovy transform @Synchronized
	@Synchronized
	public static FrameworkConfig getInstance() {
		if (instance == null) {
			instance = new FrameworkConfig();
		}
		return instance;
	}
	
	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

}
