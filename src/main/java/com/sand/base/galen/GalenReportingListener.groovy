package com.sand.base.galen

import net.mindengine.galen.reports.GalenTestInfo
import net.mindengine.galen.reports.HtmlReportBuilder

import org.testng.IReporter
import org.testng.ISuite
import org.testng.xml.XmlSuite

class GalenReportingListener implements IReporter {

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites,
			String outputDirectory) {
		println "Generating Galen HTML Reports..."
		List<GalenTestInfo> tests = GalenReportsContainer.get().getAllTests()
		
		new HtmlReportBuilder().build(tests, "target/galen-html-reports")

	}

}
